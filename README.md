# Configuração dos pacotes

## Instale com o comando:

```sh
$ pip install -r requirements.txt
```
## Executar
 
 Para executar os testes digite no diretório onde tem o arquivo main.py
 
 ```sh
 $ ./main.py test
 ```
 
 Para executar o crawler na url do desafio execute
 
 ```sh
$ ./main.py run
 ```
 
 Um arquivo será gerado chamado *epocacosmeticos.csv* no diretório raíz 

PS: Já existe o arquivo com o resultado dessa requisição com o conteúdo completo 
