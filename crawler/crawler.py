# -*- encoding:utf-8 -*-
import sys
from threading import Thread, Lock
import threading

import requests
from requests.exceptions import (
    Timeout,
    HTTPError,
    ConnectionError,
)
from validators import url as is_valid_url

from utils import HTTP_STATUS
from utils import (
    beautiful_soup_html_parser,
    random_user_agent
)


class Crawler(object):

    def __init__(self, root_url, css_selector='*', page_match_selector='*',
                 max_depth=2, number_of_workers=8):
        '''
            Quando alguma página tenha o match do page_match_selector,
            ela busca os seletores que se transformam no conteúdo da
            requisição
        '''
        self.root_url = root_url
        self.max_depth = max_depth
        self.page_match_selector = page_match_selector
        self.css_selector = css_selector
        self.number_of_workers = number_of_workers
        self.contents = {}
        self.visiteds = []
        self.targets = set()
        self.threads = []
        self.lock = Lock()
        self.MOVED_STATUS = (HTTP_STATUS.get('MOVED_PERMANENTLY'),
                             HTTP_STATUS.get('FOUND'))

    def crawl(self):
        url = None
        url = self._get_request_url(url)
        self.targets.add(url)
        self._spawn_worker()
        while self.threads:
            try:
                for thread in self.threads:
                    thread.join()
                    if not thread.isAlive():
                        self.threads.remove(thread)
            except KeyboardInterrupt:
                sys.exit()
        return self.contents

    def _spawn_worker(self):
        thread = Thread(target=self._worker)
        self.threads.append(thread)
        thread.start()

    def _worker(self):
        while self.targets:
            try:
                self.lock.acquire()
                url = self.targets.pop()
                self.visiteds.append(url)
                self._release_lock()

                headers = dict()
                headers['User-Agent'] = random_user_agent()
                if self._calc_depth(url) <= self.max_depth:
                    print(url)
                    response = requests.get(url, timeout=30, headers=headers,
                                            verify=False)
                    if response.status_code is HTTP_STATUS.get('OK'):
                            soup = beautiful_soup_html_parser(response.text)
                            self.lock.acquire()
                            match_page = soup.select(self.page_match_selector)
                            if len(match_page) > 0:
                                tags = soup.select(self.css_selector)
                                self._append_content(url, tags)
                            targets = self._get_links(soup)
                            not_visited_targets = targets - set(self.visiteds)
                            self._add_targets(not_visited_targets)
                    elif response.status_code in self.MOVED_STATUS:
                        location = response.header['location']
                        rlink = self._get_request_url(location)
                        self._add_targets([rlink])
                        continue
                if len(threading.enumerate()) <= self.number_of_workers:
                    self._spawn_worker()
            except KeyError:
                # Caso alguma thread pegue o último target
                self._release_lock()
                break
            except (Timeout, HTTPError):
                self.lock.acquire()
                self.targets.add(url)
            except ConnectionError:
               pass
            finally:
                self._release_lock()

    def _release_lock(self):
        if self.lock.locked():
            self.lock.release()

    def _get_request_url(self, url):
        if url is None:
            return self.root_url
        if self.root_url not in url:
            return self.root_url + url
        return url

    def _append_content(self, url, list_tags):
        striped_tags_content = []
        for tag in list_tags:
            striped_tags_content.append(tag.text)
        self.contents[url] = striped_tags_content
        self.contents[url].append(url)

    def _get_links(self, soup):
        links = []
        for link in soup.findAll('a', href=True):
            href = link['href']
            if (('http' in href and self.root_url in href) or
               ('http' not in href and self.root_url not in href)):
                url = self._get_request_url(href)
                try:
                    if is_valid_url(url):
                        links.append(url)
                finally:
                    '''
                        Caso dê erro lanca uma excessão de válidação que não
                        queremos tratar
                    '''
                    pass
        return set(links)

    def _add_targets(self, targets):
        for target in targets:
            self.targets.add(target)

    def _calc_depth(self, url):
        url = url.replace('https', 'http').replace(self.root_url, '')
        return len(url.rstrip('/').split('/')) - 1
