# *-* encoding: utf-8 *-*

import csv


def write_csv_file(filename, content, headers):
    with open(filename, 'wt') as csvfile:
        writer = csv.writer(csvfile)
        writer.writerow(headers)
        for key, row in content.iteritems():
            writer.writerow([unicode(s).encode("utf-8") for s in row])
        csvfile.close()
    return writer
