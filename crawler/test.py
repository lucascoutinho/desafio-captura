# -*- encoding: utf-8 -*-

import os
import unittest
from crawler import Crawler
from csv_utils import write_csv_file


class CrawlerTest(unittest.TestCase):

    def setUp(self):
        self.root_url = 'http://demo4484762.mockable.io/'

    def test_success_request_in_three_pages(self):
        crawler = Crawler(root_url=self.root_url)
        contents = crawler.crawl(url='main_page')
        self.assertEquals(len(contents), 4)

    def test_success_max_depth(self):
        crawler = Crawler(root_url=self.root_url, max_depth=2)
        contents = crawler.crawl(url='main_page_with_deep')
        self.assertEquals(len(contents), 4)

    def test_success_not_repeat_navigation(self):
        crawler = Crawler(root_url=self.root_url, max_depth=1)
        contents = crawler.crawl('main_page_test_repetead_links')
        self.assertEquals(len(contents), 4)

    def test_success_match_css_selector(self):
        crawler = Crawler(root_url=self.root_url, max_depth=1,
                          css_selector='#my_content',
                          page_match_selector='#my_content')
        contents = crawler.crawl(url='main_page_with_deep')
        index_page_1 = 'http://demo4484762.mockable.io/main_page_with_deep'
        index_page_2 = 'http://demo4484762.mockable.io/deep_1'
        self.assertTrue('Content' in contents[index_page_1])
        self.assertTrue('Content deep' in contents[index_page_2])

    def test_redirect(self):
        results = ['Page 1', 'Page 2', 'Page 3']
        crawler = Crawler(root_url=self.root_url)
        contents = crawler.crawl(url='throws_redirect')
        url = 'http://demo4484762.mockable.io/throws_redirect'
        redirect_contents = contents[url]
        self.assertEquals(results, redirect_contents)


class CsvUtilsTest(unittest.TestCase):

    def setUp(self):
        self.filename = 'test.csv'

    def test_write_content(self):
        contents = {'url1': ['Produto 1'],
                    'url2': ['Produto 2'],
                    'url3': ['Último Produto']}
        results = u'Produto 1\r\nÚltimo Produto\r\nProduto 2\r\n'
        headers = ('name',)
        write_csv_file(self.filename, contents, headers)
        with open(self.filename, 'r') as csv_file:
            content_file = csv_file.read()
            csv_file.close()
            self.assertEqual(content_file, results)
            os.remove(self.filename)

class IntegrationTest(unittest.TestCase):

    def setUp(self):
        self.root_url = 'http://demo4484762.mockable.io/'
        self.filename = 'test.csv'

    def test_crawler_and_write_file(self):
        results = 'Page 1 - content\r\nParagraph\r\nPage 1\r\nPage 2\r\nPage 3\r\n'
        crawler = Crawler(root_url=self.root_url)
        contents = crawler.crawl(url='main_page')
        self.assertEquals(len(contents), 4)
        headers = ('name',)
        write_csv_file(self.filename, contents, headers)

        with open(self.filename, 'r') as csv_file:
            content_file = csv_file.read()
            csv_file.close()
            self.assertEqual(content_file, results)
            os.remove(self.filename)


def run_tests():
    print('Starting tests')
    suite = unittest.defaultTestLoader.loadTestsFromTestCase(CrawlerTest)
    unittest.TextTestRunner().run(suite)
    suite_csv = unittest.defaultTestLoader.loadTestsFromTestCase(CsvUtilsTest)
    unittest.TextTestRunner().run(suite_csv)
    suite_in = unittest.defaultTestLoader.loadTestsFromTestCase(IntegrationTest)
    unittest.TextTestRunner().run(suite_in)
