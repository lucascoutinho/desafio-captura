# -*- encoding:utf-8 -*-
from bs4 import BeautifulSoup
from fake_useragent import UserAgent

_user_agent = UserAgent()

HTTP_STATUS = {
    'OK': 200,
    'MOVED_PERMANENTLY': 301,
    'FOUND': 302,
    'BAD_REQUEST': 400,
    'NOT_FOUND': 404,
    'INTERNAL_SERVER_ERROR': 500,
}

def beautiful_soup_html_parser(content):
    return BeautifulSoup(content, 'html.parser')

def random_user_agent():
    return _user_agent.random
