#! /usr/bin/env python

import sys
from crawler.test import run_tests
from crawler.crawler import Crawler
from crawler.csv_utils import write_csv_file


def run_application():
    contents = []
    url = 'http://www.epocacosmeticos.com.br'
    match_selector = 'div.productName'
    selectors = 'title, div.productName, strong.skuBestPrice'
    crawler = Crawler(root_url=url, css_selector=selectors,
                      page_match_selector=match_selector,
                      max_depth=2, number_of_workers=8)
    try:
        contents = crawler.crawl()
        headers = ('title', 'product', 'price', 'url')
        if contents:
            write_csv_file('epocacosmeticos.csv', contents, headers)
    except Exception as e:
        print e

if __name__ == '__main__':
    if 'test' in sys.argv:
        run_tests()
    elif 'run' in sys.argv:
        run_application()
